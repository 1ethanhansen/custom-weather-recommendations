Module {
	name: 'vetero'
	description: 'an app that gives you custom recommendations on what clothing to wear based on the weather conditions for the times of the day you care about and your preferences'
	version: '0.0.0'
	license: 'MIT'
	dependencies: []
}
