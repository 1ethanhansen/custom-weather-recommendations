module main

import vetero
import db.sqlite
import veb
import json
import time

// create_new_clothing creates a new clothing struct from json corresponding to the struct
fn create_new_clothing(json_string string) !vetero.Clothing {
	new_clothing := json.decode(vetero.Clothing, json_string) or {
		return error('BADLY FORMATTED CLOTHING JSON')
	}

	return new_clothing
}

// REST API FUNCTIONS
fn main() {
	mut app := &App{
		db: sqlite.connect('storage/users.db') or { panic('UNABLE TO CONNECT TO DB') }
	}

	app.use(veb.cors[Context](veb.CorsOptions{
		// allow CORS requests from every domain
		origins: ['*']
		// allow CORS requests with the following request methods:
		allowed_methods: [.get, .head, .patch, .put, .post, .delete]
	}))

	veb.run[App, Context](mut app, 8088)
}

pub struct App {
	veb.Middleware[Context]
pub:
	db sqlite.DB
}

pub struct Context {
	veb.Context
}

@['/account/register'; post]
pub fn (app &App) register_user(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	email := user_data['email'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply an email!')
	}
	password := user_data['password'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a password!')
	}

	if email == '' || password == '' {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('Email and password cannot be empty')
	}

	settings_id := vetero.create_new_user(email, password, app.db) or {
		return ctx.server_error('Unable to create user with email ${email}')
	}

	return ctx.text('{"settings_id": ${settings_id}}')
}

@['/account/daystart'; post]
pub fn (app &App) set_daystart(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_start := user_data['new_start'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a new_start!')
	}.int()

	println(user_data)
	println('new_start: ${new_start}')
	println('user_id: ${settings_id}')

	vetero.update_daytime_start(new_start, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/dayend'; post]
pub fn (app &App) set_dayend(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_end := user_data['new_end'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a new_end!')
	}.int()

	vetero.update_daytime_end(new_end, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/precipthresh'; post]
pub fn (app &App) set_precipthresh(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_thresh := user_data['new_thresh'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a new_thresh!')
	}.int()

	vetero.update_precip_threshold(new_thresh, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/preciptype'; post]
pub fn (app &App) set_preciptype(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_type := user_data['new_precip_type'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a new_precip_type!')
	}

	vetero.update_precip_option(new_type, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/season'; post]
pub fn (app &App) set_season(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_season := user_data['new_season'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a new_season!')
	}

	vetero.update_season(new_season, settings_id, app.db) or { return ctx.server_error(err.msg()) }

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/login'; post]
pub fn (app &App) login_user(mut ctx Context) veb.Result {
	// println('Received registration request')
	payload := ctx.Context.req.data
	// println(payload)
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	email := user_data['email'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply an email!')
	}
	password := user_data['password'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a password!')
	}
	settings_id := vetero.auth_user(email, password, app.db) or {
		return ctx.request_error(err.msg())
	}

	return ctx.text('{"settings_id": ${settings_id}}')
}

@['/account/location'; post]
pub fn (app &App) change_location(mut ctx Context) veb.Result {
	payload := ctx.Context.req.data
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_zip := user_data['zip_code'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a zip_code!')
	}

	lat, long := vetero.get_lat_long_from_zip(new_zip)

	vetero.update_loc_code('${lat},${long}', settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account/units'; post]
pub fn (app &App) change_units(mut ctx Context) veb.Result {
	payload := ctx.Context.req.data
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	new_units_str := user_data['units'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a units!')
	}

	new_units := if new_units_str == 'true' {
		true
	} else if new_units_str == 'false' {
		false
	} else {
		return ctx.request_error('units must be either true or false!')
	}

	vetero.update_temp_units(new_units, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/account'; get]
pub fn (app &App) get_account(mut ctx Context) veb.Result {
	// println('Received registration request')

	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	println(ctx.query)

	settings := vetero.load_user_settings(settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	resp_data := {
		'email':                   settings.email
		'daytime_start':           '${settings.daytime_start}'
		'daytime_end':             '${settings.daytime_end}'
		'precipitation_option':    settings.precipitation_option
		'precipitation_threshold': '${settings.precipitation_threshold}'
		'comfort_temp':            '${settings.comfort_temp}'
		'season':                  settings.season
		'location_code':           settings.location_code
		'fahrenheit':              '${settings.fahrenheit}'
	}

	return ctx.json(resp_data)
}

fn outfit_to_map(outfit []vetero.Clothing) map[string]string {
	mut resp_map := map[string]string{}
	for clothing in outfit {
		resp_map[clothing.variety] = clothing.name
	}

	return resp_map
}

@['/outfit'; get]
pub fn (app &App) retrieve_outfit(mut ctx Context) veb.Result {
	// println('Received registration request')

	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	settings := vetero.load_user_settings(settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	if vetero.check_for_history(settings_id, app.db) {
		last_hist := vetero.load_last_history(settings_id, app.db)
		now := time.now()

		if last_hist.date == now.ymmdd() {
			outfit := json.decode([]vetero.Clothing, last_hist.clothes) or {
				return ctx.server_error('Error while loading last outfit')
			}

			resp_map := outfit_to_map(outfit)

			return ctx.json(resp_map)
		}
	}

	weather := vetero.get_weather_data(settings.location_code)
	outfit, today_expected, avg_temp := vetero.get_outfit(weather, settings)
	vetero.reduce_outfit_numbers_settings(outfit, app.db)
	vetero.store_outfit_in_history(today_expected, outfit, avg_temp, settings_id, app.db)

	resp_map := outfit_to_map(outfit)

	return ctx.json(resp_map)
}

@['/exper'; get]
pub fn (app &App) exper_outfit(mut ctx Context) veb.Result {
	// println('Received registration request')

	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	settings := vetero.load_user_settings(settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	weather := vetero.get_weather_data(settings.location_code)
	outfit, _, _ := vetero.get_outfit(weather, settings)

	resp_map := outfit_to_map(outfit)

	return ctx.json(resp_map)
}

@['/feedback'; post]
pub fn (app &App) recv_feedback(mut ctx Context) veb.Result {
	payload := ctx.Context.req.data
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	yesterday_option := user_data['yesterday_option'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a yesterday_option!')
	}.int()
	yesterday_date := user_data['date'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a date!')
	}

	if !vetero.check_for_history(settings_id, app.db) {
		return ctx.server_error('USER ${settings_id} HAS NO HISTORY')
	}

	if yesterday_option !in [-1, 0, 1] {
		return ctx.request_error('yesterday_option must be in [-1, 0, 1]!')
	}

	vetero.update_clothes_learning(yesterday_option, yesterday_date, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/feedback/date'; get]
pub fn (app &App) get_feedback_date(mut ctx Context) veb.Result {
	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	if !vetero.check_for_history(settings_id, app.db) {
		return ctx.server_error('USER ${settings_id} HAS NO HISTORY')
	}

	last_hist := vetero.load_last_history(settings_id, app.db)

	if last_hist.actual != -100 {
		new_map := {
			'date': 'done'
		}
		return ctx.json(new_map)
	}

	resp_map := {
		'date': last_hist.date
	}

	return ctx.json(resp_map)
}

@['/clothes/add'; post]
pub fn (app &App) add_clothing(mut ctx Context) veb.Result {
	// expects request of JSON string like
	// '{"settings_id":1,"name":"name","variety":"shirt","heat_points":1,"waterproof":false,"hooded":false,"for_fall":true,"for_summer":true,"for_winter":true,"total_count":10,"remaining_count":10,"is_immune":false}'
	payload := ctx.Context.req.data

	new_clothing := create_new_clothing(payload) or { return ctx.request_error(err.msg()) }
	vetero.add_to_wardrobe(new_clothing, app.db) or { return ctx.server_error(err.msg()) }

	return ctx.text('{"status": "SUCCESS"}')
}

@['/clothes/edit'; post]
pub fn (app &App) edit_clothing(mut ctx Context) veb.Result {
	// expects request of JSON string like
	// '{"settings_id":1,"name":"name","variety":"shirt","heat_points":1,"waterproof":false,"hooded":false,"for_fall":true,"for_summer":true,"for_winter":true,"total_count":10,"remaining_count":10,"is_immune":false}'
	payload := ctx.Context.req.data

	vetero.update_specific_clothing(payload, app.db) or { return ctx.server_error(err.msg()) }

	return ctx.text('{"status": "SUCCESS"}')
}

@['/clothes/delete'; post]
pub fn (app &App) delete_clothing(mut ctx Context) veb.Result {
	payload := ctx.Context.req.data
	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}
	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()
	clothing_id := user_data['clothing_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a yesterday_option!')
	}.int()

	vetero.rm_from_wardrobe(clothing_id, settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	return ctx.text('{"status": "SUCCESS"}')
}

@['/clothes'; get]
pub fn (app &App) get_clothing(mut ctx Context) veb.Result {
	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	clothing := vetero.get_clothes(settings_id, app.db) or { return ctx.server_error(err.msg()) }

	return ctx.json(clothing)
}

@['/clothes/wash'; post]
pub fn (app &App) wash_clothing(mut ctx Context) veb.Result {
	payload := ctx.Context.req.data

	user_data := json.decode(map[string]string, payload) or {
		ctx.res.set_status(.bad_request)
		return ctx.redirect('/', typ: .see_other)
	}

	settings_id := user_data['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	vetero.reset_clothes_numbers(settings_id, app.db) or { return ctx.server_error(err.msg()) }
	return ctx.text('{"status": "SUCCESS"}')
}

@['/weathersumm'; get]
pub fn (app &App) get_weathersumm(mut ctx Context) veb.Result {
	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	settings := vetero.load_user_settings(settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	all_weather := vetero.get_weather_data(settings.location_code)
	day_weather := vetero.remove_unwanted_times(all_weather, settings)

	weather_summ := vetero.get_weather_summ(day_weather, settings)

	return ctx.json(weather_summ)
}

@['/history'; get]
pub fn (app &App) get_history_list(mut ctx Context) veb.Result {
	// Get the settings_id from the query parameters
	settings_id := ctx.query['settings_id'] or {
		ctx.res.set_status(.bad_request)
		return ctx.request_error('You have to supply a settings_id!')
	}.int()

	// Retrieve the history list for the settings
	history := vetero.get_history_list(settings_id, app.db) or {
		return ctx.server_error(err.msg())
	}

	// Map the History list to the HistorySummary struct
	history_summary := history.map(fn (item vetero.History) vetero.HistorySummary {
		return vetero.HistorySummary{
			date:     item.date
			expected: item.expected
			actual:   item.actual
		}
	})

	// Return the history summary list as a JSON response
	return ctx.json(history_summary)
}
