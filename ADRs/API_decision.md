# Deciding on Which Weather API to Use

## Context and Problem Statement

There are several options for free weather APIs so deciding on one that fulfills our requirements was needed.

## Considered Options

- openweathermap: free tier only gives forecast every 3 hours
- weatherbit: no free tier
- AccuWeather: 12 hour hourly forecast
- tomorrow.io: uncertain about hourly forecasts
- weather 2020 need to request a demo
- visualcrossing: uncertain about hourly forecasts
- aeris: no free tier

## Decision Outcome

### Chosen option: "AccuWeather"
because it has a free 12 hour hourly forecast, and it works reliably in our daily life experience.

* Good, because it has a free package
* Good, because it has a specified 12 hour hourly forecast for specific locations
* Good, because It give a forecast for 5 consecutive days
* Good, because it works reliably based on our experience

### Tomorrow.io
Our second best option. We did not choose it because the free package does not specify the length of the hourly forecast.

* Good, because it has a free package
* Good, because it has an automatically monitored location
* Bad, because it has a weather forecast, however it does specify the lenght of the amount of hours for the hourly forecast.
