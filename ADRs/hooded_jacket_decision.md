should be a "hooded" bool
if a jacket with a hood is selected:
	1. make "none" hat temporarily waterproof
	2. choose to add 1/5 of the heat points from the selected jacket or not

What is the problem?
1. when a hooded item is selected, sometimes it also adds a waterproof hat even though the hood serves that function
2. the heat points don't account for the hood of the selected item
(#1 is the main problem, #2 is secondary)

options:
1. hood doesn't add any heat points
	- pro: simple
	- con: minimal added information
	- rejected because this can be approximated by making % in #4 very small
2. hood creates a temporary item that's a hat w/ certain amount of heat points
	- con: hard to track over time
	- pro: more info
	- rejected because I cannot see a clean solution to tracking over time
3. hood adds a fixed number of heat points
	- pro: simple
	- con: different hoods should add different heat points
4. hood adds a fixed percentage of the selected item's heat points
	- pro: simple
	- selected because it's a good mix of added information and simplicity in implementation
5. every time a hooded item is added, a permanent item called ${name}-hood is also created
	- overkill