from graphviz import Digraph
import matplotlib.pyplot as plt
from collections import Counter
import sys
from datetime import datetime

def create_dag(input_file):
    """
    Create a Directed Acyclic Graph visualization from an input file.
    Input file should contain edges, one per line, with space-separated vertices.
    """
    # Initialize a new directed graph
    dot = Digraph(comment='Directed Acyclic Graph')
    dot.attr(rankdir='LR')  # Left to right layout
    
    # Set of all nodes to track unique vertices
    nodes = set()
    # List to store all numbers for histogram
    all_numbers = []
    
    try:
        with open(input_file, 'r') as f:
            # Read each line and add edges to the graph
            for line in f:
                # Split line into source and target nodes
                source, target = line.strip().split()
                
                # Add nodes if they don't exist
                if source not in nodes:
                    dot.node(source, source)
                    nodes.add(source)
                if target not in nodes:
                    dot.node(target, target)
                    nodes.add(target)
                
                # Add edge
                dot.edge(source, target)

                source = float(source)
                target = float(target)

                # Add to numbers list for histogram
                all_numbers.extend([source, target])
        
        # Save the graph
        output_file = 'dag_output' + datetime.today().strftime('%d.%m.%Y')
        dot.render(output_file, view=False, format='pdf')
        print(f"Graph has been saved as '{output_file}.pdf'")

        # Create histogram
        counter = Counter(all_numbers)
        values = list(counter.keys())
        counts = list(counter.values())
        
        plt.figure(figsize=(10, 6))
        plt.bar(values, counts)
        plt.title('Frequency of Node Values')
        plt.xlabel('Node Value')
        plt.ylabel('Frequency')
        plt.grid(True, alpha=0.3)

        # Set x-axis to log scale
        plt.xscale('log')
        
        # Save histogram
        plt.savefig('node_histogram.pdf')
        print("Histogram has been saved as 'node_histogram.pdf'")
        
    except FileNotFoundError:
        print(f"Error: File '{input_file}' not found.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python dag.py <input_file>")
        sys.exit(1)
    
    input_file = sys.argv[1]
    create_dag(input_file)