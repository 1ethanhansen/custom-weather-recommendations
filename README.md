# Vetero

Vetero is an app that gives you custom recommendations on what clothing to wear based on the weather conditions for the times of the day you care about and your preferences.

Vetero is also the [Esperanto](https://lernu.net/) word for "[weather](https://en.bab.la/dictionary/esperanto-english/vetero)."

## How to use Vetero

To run Vetero at all, you can use the pre-built binary and run it as a command. Or you can clone the repository and from the same level where `vet.v` is located, run `v run vet.v`.

### Initial Setup

In order to set up Vetero, you need to make sure a couple preferences are correct

1. First, you'll need an [API key for AccuWeather](https://developer.accuweather.com/) to get the weather data. You can store that API key for the program to use by [running Vetero](#how-to-use-vetero) and choosing the `(S)et API key` option in the main menu. You can do this by typing `s` or `S`, either works, then pressing `enter`
1. Next, you'll want to set your location. Do this by [running Vetero](#how-to-use-vetero), then `u`, then `l`. Enter your zip code to change location to that zip code.
1. Finally, you need to add in types of clothing you own. You don't need to do one entry for each actual piece of clothing, but for each type. For instance, I have 6 pairs of sweatpants, but I'd only add in one entry to Vetero for "sweatpants."
  - To make sure you don't accidentally add in an item that already exists, [run Vetero](#how-to-use-vetero) then `v` to view wardrobe.
  - Then to actually add clothes, [run Vetero](#how-to-use-vetero) then `u` then `a` for (A)dd new clothing
  - select the appropriate variety the clothing you want falls under (type the full name!)
  - select a guess amount of heat points (don't worry, the program will update this later!) Enter a whole number. To view some guidelines, refer back to the printed-out wardrobe which has all that info. It doesn't matter too much that the actual number is right, as long as the relative number is. For instance heat points of a winter coat should be greater than a sweater should be greater than a t-shirt.
  - then enter `y` or `n` for if it is waterproof or not
  - finally enter the specific name (again, see the wardrobe printout for some examples)
4. repeat that for all the clothes you want to add and all of the given options

### Other Options

That is all of the _required_ settings to change in order to get Vetero working. There are some more changes that you'd probably like to customize, and you can do that by [running Vetero](#how-to-use-vetero), then `u` for update.

- `D`aytime start: enter a whole number. This is when you're likely to be out and about. enter `7` for 7am, etc
- `N`ighttime start: enter a whole number. This is when you're likely to be back for the day. Enter `22` for 10pm, `23` for 11pm, etc
- `P`recipitation threshold: enter a whole number. This is the minimum value for it to be considered a "rainy/snowy" day
- `T`ype of precipitation: enter either `SUM` or `MAX`. `SUM` means that a day will be considered rainy/snowy if the sum total of all chances of precipitation exceeds the threshold. `MAX` means that a day will be considered rainy/snowy if ANY chance of precipitation exceeds the threshold.

### Daily Use

Once Vetero is set up (see [the sections above](#initial-setup)), using it is as easy as [running Vetero](#how-to-use-vetero) and selecting `g` to get the recommended outfit for the day.

#### Algorithm feedback

When you first [run Vetero](#how-to-use-vetero) each time, it will ask for feedback on how it did yesterday. This is how the algorithm learns your preferences over time. Follow the on-screen instructions to tell it how you felt the day before. If it has been a few days or you don't care, you can enter `s` for "skip".

## Motivation + how it works

Most of the time, when you check the weather you don't care if the low temperature for the day happens at 2 in the morning - you'll be asleep then. So what clothes should you wear if you want to be the most comfortable for the largest part of the day that you're out and about? This is the problem Vetero is designed to solve.

A user will be able to specify what sort of clothes they have and under what conditions they want to wear them. For instance, if the average temperature for the day will be 15-20°C you might want to wear long pants with a short sleeve shirt. But if it's also raining you should take a jacket. Vetero will combine all this data and decide for you so you don't have to think too hard about it.

## Some technical stuff

### Why we're using V

Vetero is is written in [V](https://vlang.io/) because it is a nice language to work with: fast, modern, and it is good to learn new things. Arguments for why V is a nice language include:
- Safety (no null and immutable variables by default for instance)
- Simplicity
- Easy to read
- As fast as C
- Fast compilation
- Runs anywhere C runs
- Built in testing framework
- Great error messages

### What API we use for weather

We will be using [AccuWeather](https://developer.accuweather.com/apis) for now as documented in our [ADR](./ADRs/API_decision.md)

### Contributing info

We attempt to use a few things in our development process:
- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Conventional Comments](https://www.conventionalcomments.org)
- [Semantic Versioning](https://semver.org)

If you're going to contribute, make a fork, make your commits, then make a merge request. Usually smaller changes are easier to review and more likely to be approved in a timely manner. We'd rather you open 3 merge requests each addressing 1 issue than 1 merge request addressing 3 issues.

### License

Vetero is licensed under the [MIT license](./LICENSE)