module vetero

import json
import rand { bernoulli }
import db.sqlite
import time

// new struct clothing history
// date, int expected (too hot, too cold, just right), int actual
// string which is JSON for the clothes that day. Can't just make it reference
//   other clothes in the table, because their heat points (&etc) will change
// avg_temp stores the predicted average temp for that day
@[table: 'clothing_history']
pub struct History {
pub:
	hist_id     int @[primary; sql: serial]
	settings_id int
	date        string
	expected    int
	actual      int
	clothes     string
	avg_temp    f64
}

// Settings is a general struct for storing all the user's preferences
@[table: 'user_settings']
pub struct Settings {
pub:
	id                      int    @[primary; sql: serial]
	email                   string @[unique]
	password                string
	daytime_start           int    = 8
	daytime_end             int    = 23
	precipitation_option    string = 'MAX'
	precipitation_threshold int    = 51
	comfort_temp            f64    = 25.0
	c_temp_change_rate      f64    = 1
	season                  string = 'winter'
	location_code           string = '41.5084,-81.6076'
	fahrenheit              bool
	wardrobe                []Clothing @[fkey: 'settings_id']
	history_list            []History  @[fkey: 'settings_id']
}

// The Clothing struct contains info about each clothing option the user inputs
@[table: 'clothing']
pub struct Clothing {
	clothing_id    int @[primary; sql: serial]
	hp_change_rate f64 = 0.5
pub:
	settings_id     int
	name            string
	variety         string
	heat_points     f64
	waterproof      bool
	hooded          bool
	for_fall        bool = true
	for_summer      bool = true
	for_winter      bool = true
	total_count     int  = 1
	remaining_count int  = 1
	is_immune       bool
}

@[table: 'clothing']
pub struct HistorySummary {
pub:
	date     string
	expected int
	actual   int
}

// load_user_settings gets the Settings object for a specific user ID
pub fn load_user_settings(user_id int, db sqlite.DB) !Settings {
	// get the Settings from the database
	user_settings := sql db {
		select from Settings where id == user_id limit 1
	} or { return error('UNABLE TO LOAD USER WITH ID ${user_id}') }

	if user_settings.len == 0 {
		return error('USER NOT FOUND IN DB')
	}

	return user_settings.first()
}

// store_outfit_in_history stores the suggested outfit for the day, along with some metadata
pub fn store_outfit_in_history(expected int, prev_outfit []Clothing, temp f64, user_id int, db sqlite.DB) {
	// We don't want to just have the clothing_ids because those clothes will change in the database
	//   and we want to be able to know exactly what was suggested on each day and why. So we freeze
	//   the clothes to a json string and save that in the History
	clothes_json := json.encode(prev_outfit)

	now := time.now()

	new_history := History{
		settings_id: user_id
		date:        now.ymmdd()
		expected:    expected
		actual:      -100 // make this a nonsense number so we can easily
		clothes:     clothes_json
		avg_temp:    temp
	}
	sql db {
		insert new_history into History
	} or { panic('Error inserting history into the database') }
}

// check_for_history makes sure that at least some history exists before asking for feedback
pub fn check_for_history(user_id int, db sqlite.DB) bool {
	history := sql db {
		select from History where settings_id == user_id
	} or { return false }

	if history.len > 0 {
		return true
	}
	return false
}

// load_last_history retrieves a user's most recent history from the database
pub fn load_last_history(user_id int, db sqlite.DB) History {
	history := sql db {
		select from History where settings_id == user_id order by date desc limit 1
	} or { panic('Failed to retrieve most recent outfit') }

	return history[0]
}

// load_date_history retrieves a user's most history on a certain date
pub fn load_date_history(user_id int, date string, db sqlite.DB) !History {
	history := sql db {
		select from History where settings_id == user_id && date == date order by date desc limit 1
	} or { return error('Failed to retrieve outfit on date ${date}') }

	if history.len > 0 {
		return history[0]
	}

	return error('No history found with date ${date}')
}

// load_prev_outfit decodes the JSON string stored in a History into an array of Clothing structs
pub fn load_prev_outfit(history History) []Clothing {
	decoded_clothes := json.decode([]Clothing, history.clothes) or {
		panic('UNABLE TO EXTRACT OUTFIT')
	}

	return decoded_clothes
}

//
pub fn store_yesterdays_actual(actual int, yesterday History, db sqlite.DB) {
	sql db {
		update History set actual = actual where hist_id == yesterday.hist_id
	} or { panic("UNABLE TO UPDATE YESTERDAY'S ACTUAL FEEL") }
}

// *******************************************
// Simple update functions for changing the DB
// *******************************************
pub fn update_daytime_start(new_start int, user_id int, db sqlite.DB) ! {
	sql db {
		update Settings set daytime_start = new_start where id == user_id
	} or { return error('COULD NOT CHANGE START TIME TO ${new_start}') }
}

pub fn update_daytime_end(new_end int, user_id int, db sqlite.DB) ! {
	sql db {
		update Settings set daytime_end = new_end where id == user_id
	} or { return error('COULD NOT CHANGE END TIME TO ${new_end}') }
}

pub fn update_loc_code(new_loc_code string, user_id int, db sqlite.DB) ! {
	sql db {
		update Settings set location_code = new_loc_code where id == user_id
	} or { return error('COULD NOT UPDATE LOCATION CODE') }
}

pub fn update_precip_option(new_precip_option string, user_id int, db sqlite.DB) ! {
	if new_precip_option !in ['MAX', 'SUM'] {
		return error('precipitation option must be either "MAX" OR "SUM"')
	}

	sql db {
		update Settings set precipitation_option = new_precip_option where id == user_id
	} or { return error('COULD NOT CHANGE OPTION TO ${new_precip_option}') }
}

pub fn update_precip_threshold(new_precip_threshold int, user_id int, db sqlite.DB) ! {
	sql db {
		update Settings set precipitation_threshold = new_precip_threshold where id == user_id
	} or { return error('COULD NOT CHANGE THRESHOLD TO ${new_precip_threshold}') }
}

pub fn update_comfort_temperature(new_temp f64, user_id int, db sqlite.DB) {
	sql db {
		update Settings set comfort_temp = new_temp where id == user_id
	} or { panic('COULD NOT UPDATE COMFORT TEMPERATURE') }
}

pub fn update_c_temp_change_rate(new_rate f64, user_id int, db sqlite.DB) {
	sql db {
		update Settings set c_temp_change_rate = new_rate where id == user_id
	} or { panic('COULD NOT UPDATE COMF TEMP CHANGE RATE') }
}

pub fn update_season(new_season string, user_id int, db sqlite.DB) ! {
	if new_season !in ['summer', 'fall', 'winter'] {
		return error('season must be one of "summer", "fall", "winter"')
	}

	sql db {
		update Settings set season = new_season where id == user_id
	} or { return error('COULD NOT CHANGE SEASON TO ${new_season}') }
}

pub fn update_api_key(new_key string, user_id int, db sqlite.DB) bool {
	return true
}

pub fn update_specific_clothing(new_json_string string, db sqlite.DB) ! {
	intermediate_clothing := json.decode(Clothing, new_json_string) or {
		return error('BADLY FORMATTED JSON STRING')
	}
	this_id := intermediate_clothing.clothing_id

	sql db {
		update Clothing set name = intermediate_clothing.name where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING NAME') }

	sql db {
		update Clothing set variety = intermediate_clothing.variety where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING VARIETY') }

	sql db {
		update Clothing set heat_points = intermediate_clothing.heat_points where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING heat_points') }

	sql db {
		update Clothing set waterproof = intermediate_clothing.waterproof where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING waterproof') }

	sql db {
		update Clothing set hooded = intermediate_clothing.hooded where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING hooded') }

	sql db {
		update Clothing set for_fall = intermediate_clothing.for_fall where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING for_fall') }

	sql db {
		update Clothing set for_summer = intermediate_clothing.for_summer where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING for_summer') }

	sql db {
		update Clothing set for_winter = intermediate_clothing.for_winter where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING for_winter') }

	sql db {
		update Clothing set total_count = intermediate_clothing.total_count where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING total_count') }

	sql db {
		update Clothing set remaining_count = intermediate_clothing.total_count where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING remaining_count') }

	sql db {
		update Clothing set is_immune = intermediate_clothing.is_immune where clothing_id == this_id
	} or { return error('UNABLE TO CHANGE CLOTHING is_immune') }
}

pub fn update_temp_units(new_choice bool, user_id int, db sqlite.DB) ! {
	sql db {
		update Settings set fahrenheit = new_choice where id == user_id
	} or { return error('UNABLE TO UPDATE YOUR UNITS PREFERENCE') }
}

// add_to_wardrobe adds some new clothes to the currently stored wardrobe
// currently assumes that each clothing item already has the user's id in settings_id
pub fn add_to_wardrobe(clothes_to_add Clothing, db sqlite.DB) ! {
	sql db {
		insert clothes_to_add into Clothing
	} or { return error('UNABLE TO ADD NEW CLOTHING ${clothes_to_add.name}') }
}

pub fn rm_from_wardrobe(to_delete int, user_id int, db sqlite.DB) ! {
	sql db {
		delete from Clothing where clothing_id == to_delete && settings_id == user_id
	} or { return error('UNABLE TO DELETE CLOTHING') }
}

// load_api_key loads the accuweather key from the user's Settings
pub fn load_api_key(stored_settings Settings) string {
	return ''
}

// get_start_time just a way to get daytime start time
pub fn get_start_time(stored_settings Settings) int {
	return stored_settings.daytime_start
}

// get_end_time just a way to get daytime end time
pub fn get_end_time(stored_settings Settings) int {
	return stored_settings.daytime_end
}

// get_precipitation_option just a way to get the option to display
pub fn get_precipitation_option(stored_settings Settings) string {
	return stored_settings.precipitation_option
}

// get_precipitation_threshold just a way to get the threshold to display
pub fn get_precipitation_threshold(stored_settings Settings) int {
	return stored_settings.precipitation_threshold
}

// gets current season
pub fn get_season(stored_settings Settings) string {
	return stored_settings.season
}

// update_clothes_learning lets Vetero respond to the user's feedback that they felt just right, too hot,
// 	 or too cold the day before.
// no returns, just store the updated settings to the file at the end
pub fn update_clothes_learning(actual int, date string, user_id int, db sqlite.DB) ! {
	settings := load_user_settings(user_id, db) or { return err }

	last_hist := load_date_history(user_id, date, db) or { return err }
	yesterday_outfit := load_prev_outfit(last_hist)
	expected := last_hist.expected

	store_yesterdays_actual(actual, last_hist, db)

	// if Vetero expected the user to be too X, and they reported Y, we treat it differently
	//   depending on whether Vetero was right or wrong. For instance, if Vetero said "the temp
	//   outside is 40C, no matter what you're going to be too hot", and the user said "too hot"
	//   then Vetero was right.
	// Here's a transition table for this logic:
	// | expected | actual | treat as    |
	// | -------- | ------ | ----------- |
	// | hot      | hot    | right       |
	// | hot      | right  | almost_cold |
	// | hot      | cold   | cold        |
	// | right    | hot    | hot         |
	// | right    | right  | right       |
	// | right    | cold   | cold        |
	// | cold     | hot    | hot         |
	// | cold     | right  | almost_hot  |
	// | cold     | cold   | right       |
	// cold = -1; right = 0; hot = 1
	// almost right means treat as hot/cold, but with a significantly reduced change rate

	mut treat_as := 'right'

	if actual == expected {
		treat_as = 'right'
	} else if expected == -1 && actual == 0 {
		treat_as = 'almost hot'
	} else if expected == 1 && actual == 0 {
		treat_as = 'almost cold'
	} else if actual < expected {
		treat_as = 'cold'
	} else {
		treat_as = 'hot'
	}

	println('Yesterday Vetero predicted you would feel ${expected}. Your feedback just said you felt ${actual}. Vetero will treat yesterday\'s outfit as ${treat_as}')

	new_c_temp_change_rate := match treat_as {
		'right', 'almost hot', 'almost cold' {
			settings.c_temp_change_rate / 2
		}
		else {
			// if it was wrong, we want to increase change rate usually only when
			// the change rate is low. If it's high, we want a small probability of it changing
			if bernoulli(1.0 / settings.c_temp_change_rate) or { true } {
				settings.c_temp_change_rate * 2
			} else {
				settings.c_temp_change_rate
			}
		}
	}
	update_c_temp_change_rate(new_c_temp_change_rate, user_id, db)

	new_c_temp := match treat_as {
		'hot' { settings.comfort_temp - new_c_temp_change_rate }
		'cold' { settings.comfort_temp + new_c_temp_change_rate }
		'almost hot' { settings.comfort_temp - (new_c_temp_change_rate / 2) }
		'almost cold' { settings.comfort_temp + (new_c_temp_change_rate / 2) }
		else { settings.comfort_temp }
	}
	update_comfort_temperature(new_c_temp, user_id, db)

	for item in yesterday_outfit {
		if treat_as in ['right', 'almost cold', 'almost hot'] {
			new_hp_change_rate := item.hp_change_rate / 2
			sql db {
				update Clothing set hp_change_rate = new_hp_change_rate where clothing_id == item.clothing_id
			} or { return error('PLEASE REPORT ERROR CODE 001') }

			if treat_as == 'almost hot' {
				new_hp := item.heat_points + (new_hp_change_rate / 2)
				sql db {
					update Clothing set heat_points = new_hp where clothing_id == item.clothing_id
				} or { panic('ERROR CODE 005') }
			} else if treat_as == 'almost cold' {
				new_hp := item.heat_points - (new_hp_change_rate / 2)
				sql db {
					update Clothing set heat_points = new_hp where clothing_id == item.clothing_id
				} or { panic('ERROR CODE 006') }
			}
		} else {
			// if it was wrong, we want to increase change rate usually only when
			// the change rate is low. If it's high, we want a small probability of it changing
			if bernoulli(1.0 / item.hp_change_rate) or { true } {
				new_hp_change_rate := item.hp_change_rate * 2
				sql db {
					update Clothing set hp_change_rate = new_hp_change_rate where clothing_id == item.clothing_id
				} or { return error('PLEASE REPORT ERROR CODE 002') }
			}
			// if we lost that random toss, don't do anything!

			if treat_as == 'hot' {
				new_hp := item.heat_points + item.hp_change_rate
				sql db {
					update Clothing set heat_points = new_hp where clothing_id == item.clothing_id
				} or { return error('PLEASE REPORT ERROR CODE 003') }
			} else if treat_as == 'cold' {
				new_hp := item.heat_points - item.hp_change_rate
				sql db {
					update Clothing set heat_points = new_hp where clothing_id == item.clothing_id
				} or { return error('PLEASE REPORT ERROR CODE 004') }
			}
		}
	}
}

// reduce_outfit_numbers_settings goes through all of the Clothing its given and reduces the
//   number that the DB says are available to the user
pub fn reduce_outfit_numbers_settings(outfit []Clothing, db sqlite.DB) {
	for item in outfit {
		if !item.is_immune {
			new_count := item.remaining_count - 1
			sql db {
				update Clothing set remaining_count = new_count where clothing_id == item.clothing_id
			} or { panic('UNABLE TO REDUCE AVAILABLE COUNT FOR ${item}') }
		}
	}
}

// reset_clothes_numbers resets all of a user's clothes back to fully available
pub fn reset_clothes_numbers(user_id int, db sqlite.DB) ! {
	mut settings := load_user_settings(user_id, db) or { return err }

	for item in settings.wardrobe {
		sql db {
			update Clothing set remaining_count = item.total_count where clothing_id == item.clothing_id
		} or { panic('UNABLE TO RESET AVAILABLE COUNT FOR ${item}') }
	}
}

// create_default_wardrobe creates the default wardrobe consisting of a "none" for most varieties
pub fn create_default_wardrobe() []Clothing {
	none_varieties := ['hat', 'outershirt', 'gloves', 'baseleg', 'baseshirt', 'outerleg', 'scarf',
		'midshirt']
	// These clothes you always have to have on: socks, shoes, shirt, pants
	mut wardrobe := []Clothing{}
	for variety in none_varieties {
		wardrobe << Clothing{
			name:      'none'
			variety:   variety
			is_immune: true
		}
	}
	return wardrobe
}

// create_new_user makes a new user with given email and password in the DB
pub fn create_new_user(new_email string, new_password string, db sqlite.DB) !int {
	new_user := Settings{
		email:    new_email
		password: new_password
		wardrobe: create_default_wardrobe()
	}
	// println(new_user)
	sql db {
		insert new_user into Settings
	} or { return error('FAILED TO CREATE USER ${new_email}: ${err}') }

	return find_user_id_by_email(new_email, db)
}

// find_user_id_by_email returns the int user_id after finding the user in the DB
pub fn find_user_id_by_email(email string, db sqlite.DB) !int {
	result := sql db {
		select from Settings where email == email
	} or { return error('UNABLE TO FIND USER WITH EMAIL ${email}') }

	if result.len == 0 {
		return error('COULDNT FIND USER WITH EMAIL ${email} IN DB')
	}

	user_id := result.first().id

	return user_id
}

// auth_user will make sure the user's email and password match and give the user ID
pub fn auth_user(email string, password string, db sqlite.DB) !int {
	result := sql db {
		select from Settings where email == email
	} or { return error('DB ERROR: UNABLE TO FIND USER WITH EMAIL ${email}') }

	if result.len < 1 {
		return error('NO USER WITH EMAIL ${email}')
	}

	settings := result.first()

	if settings.password == password {
		return settings.id
	} else {
		return error('INCORRECT PASSWORD')
	}
}

pub fn get_clothes(settings_id int, db sqlite.DB) ![]Clothing {
	wardrobe := sql db {
		select from Clothing where settings_id == settings_id
	} or { return error('Error querying clothes table') }
	return wardrobe
}

pub fn get_history_list(settings_id int, db sqlite.DB) ![]History {
	// Load the user settings for the given settings ID
	user_settings := load_user_settings(settings_id, db) or {
		return error('Failed to load user settings for settings ID: ${settings_id}')
	}

	if user_settings.history_list.len > 100 {
		return user_settings.history_list#[-100..]
	}

	// Return the history list associated with the settings
	return user_settings.history_list
}
