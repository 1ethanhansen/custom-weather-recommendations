module vetero

import regex
import net.http
import json
import encoding.csv
import os
import arrays

struct IntermediateWeather {
	date_time                 string
	temperature               int
	precipitation_probability Precipitation
	short_forecast            string
	detailed_forecast         string
}

struct Precipitation {
	value int
}

struct Periods {
	periods []IntermediateWeather
}

// Weather is a general struct for storing all the relevant weather data in an hour
struct Weather {
	date_time                 string
	temperature               int
	precipitation_probability int
	short_forecast            string
}

struct City {
	zip   string
	city  string
	state string
	lat   string
	long  string
}

// get_lat_long_from_zip uses a local CSV to convert from a zip code to latitude and longitude
// Geolocation info from https://download.geonames.org/export/zip/
// postal_code: given by the user
// returns: latitude and longitude separately
pub fn get_lat_long_from_zip(zip string) (string, string) {
	file_path := os.abs_path('storage/US.csv')
	csv_string := os.read_file(file_path) or { panic('unable to read storage/US.csv') }

	cities := csv.decode[City](csv_string)

	city := arrays.find_first(cities, fn [zip] (i City) bool {
		return i.zip == zip
	}) or { panic('${zip} is an invalid zip code') }

	return city.lat, city.long
}

// Cleans and formats raw weather data by converting camelCase keys to snake_case,
// removes unnecessary entries, and prepares it for parsing.
fn clean_data(raw_data string) string {
	mut file := raw_data

	// changing the format of desired weather data from camel case to snake case so that parsing works
	camel := [r'startTime', r'probabilityOfPrecipitation', r'detailedForecast', r'shortForecast']
	snake := ['date_time', 'precipitation_probability', 'detailed_forecast', r'short_forecast']

	for i, query in camel {
		mut re := regex.regex_opt(query) or { panic('failed to find camel case') }
		file = re.replace_simple(file, snake[i])
	}

	mut re_one := regex.regex_opt('"number": 26.*') or {
		panic('failed to create regex for number 26')
	}
	file = re_one.replace_simple(file, '')

	// remove some other entries that can screw stuff up
	remove_patterns := [r'"number":.*,', r'"name":.*,', r'"endTime":.*,', '"temperatureTrend":.*,',
		r'"dewpoint":.*"icon":.*",', r'"isDaytime":.*,', r'"temperatureUnit":.*,', r'"unitCode":.*,']
	for pattern in remove_patterns {
		mut re := regex.regex_opt(pattern) or { panic('failed to create regex for ${pattern}') }
		file = re.replace_simple(file, '')
	}

	mut re_two := regex.regex_opt(r'"@context": \[.*"periods": \[') or {
		panic('failed to create regex for "@context:".*"periods": [')
	}
	file = re_two.replace_simple(file, '"periods": [')

	file = file#[..-32]
	file += ']}'

	return file
}

// fahrenheit_to_celsius converts temperature to celsius
fn fahrenheit_to_celsius(fahrenheit int) int {
	return int((fahrenheit - 32) * 5 / 9)
}

/**
 * Converts a `Periods` struct into an array of `Weather` structs.
 *
 * This function takes a `Periods` struct containing weather data and parses
 * it into an array of `Weather` structs. Each `Weather` struct is created
 * from the corresponding weather data within the `periods` of the input.
 *
 * @param period_struct: A `Periods` struct that contains an array of weather
 *                       data periods to be converted.
 *
 * @return An array of `Weather` structs, each representing the weather data
 *         for a specific time period.
 */

fn parse_into_weather(period_struct Periods) []Weather {
	mut weathers := []Weather{}
	for intermediate_weather in period_struct.periods {
		// make new weather
		weather := Weather{
			date_time:                 intermediate_weather.date_time
			temperature:               fahrenheit_to_celsius(intermediate_weather.temperature)
			precipitation_probability: intermediate_weather.precipitation_probability.value
			short_forecast:            intermediate_weather.short_forecast
		}
		weathers << weather
	}
	return weathers
}

/**
 * Extracts the forecast horuly URL from the given weather data response.
 *
 * This function retrieves the text from the specified URL and searches for
 * the "forecastHourly" key in the response. It extracts and returns the
 * associated URL for the hourly weather forecast.
 *
 * @param url: A string representing the URL to fetch the weather data from.
 *
 * @return A string containing the forecast hourly URL extracted from the response.
 *         Returns an empty string if the forecast URL cannot be found.
 */
fn extract_forecast_url_from_text(url string) string {
	mut text := http.get_text(url)

	mut forecast_url := ''

	// Split the string into lines using split_into_lines
	lines := text.split_into_lines()
	// Output the result
	for line in lines {
		substr := 'forecastHourly'
		contains_substr := line.contains(substr)
		if contains_substr {
			key := '"forecastHourly": '

			key_index := line.index(key) or {
				println('The key "${key}" was not found in the line.')
				return ''
			}
			// Find the position of the URL after the key
			url_start := line.index_after('"', key_index + key.len) + 1
			url_end := line.index_after('"', url_start)

			// Extract the URL
			forecast_url = line[url_start..url_end]
			break
		}
	}
	return forecast_url
}

/**
 * Retrieves weather data for the next 24 hours based on the specified location.
 *
 * @param location: A string representing the geographic coordinates in the format "longitude,latitude"
 *                  (e.g., "37.7749,-122.4194"). This indicates the point for which to fetch weather data.
 *
 * @return An array of Weather objects representing the weather forecast for the next 24 hours,
 *         parsed from the weather API response.
 */
pub fn get_weather_data(location string) []Weather {
	base_url := 'https://api.weather.gov/points/'
	full_url := base_url + location
	hourly_url := extract_forecast_url_from_text(full_url)
	mut file := http.get_text(hourly_url)
	file = clean_data(file)
	period_struct := json.decode(Periods, file) or { panic('Unable to parse into periods struct') }
	weather := parse_into_weather(period_struct)
	return weather
}

// get_location_code requests and isolates the location from accuweather based on the user's zip code
// if location code is not found an empty string will be returned
// api_key: used to access accuweather's database
// postal_code: relative to the user
// returns: accuweather's location code as a string
// DEPRECATED
// pub fn get_location_code(postal_code string, api_key string) string {
// 	// url that gets weather based on postal code
// 	base_url := 'http://dataservice.accuweather.com/locations/v1/postalcodes/search'
// 	request_url := '?apikey=' + api_key + '&q=' + postal_code
// 	url := base_url + request_url

// 	// returns a string with the location information
// 	file := http.get_text(url)

// 	query := r'"Key":'
// 	mut re := regex.regex_opt(query) or { panic('failed to find key') }
// 	_, end := re.find(file)

// 	// println(file)

// 	// returns the location code
// 	return file[end + 1..end + 9]
// }

// get_weather_data requests weather data and stores it in weather structs
// if it fails to retrieve, reformat, or parse weather data it will return an empty string
// api_key: used to access accuweather's database
// location: accuweather's location key relative to the user
// returns: a weather struct with the reformatted weather data
// DEPRECATED
// pub fn get_weather_data(location string, api_key string) []Weather {
// 	mut file := get_data(location, api_key)
// 	file = clean_data(file)
// 	weather := parse_into_weather(file)
// 	return weather
// }
