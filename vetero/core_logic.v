module vetero

import time
import math
import arrays
import rand
import os

pub struct WeatherSummary {
	avg_temp        f64
	precip_bool     bool
	forecast_phrase string
	fahrenheit      bool
}

// remove_unwanted_times removes weather at the user doesn't care
// day_weather: array of Weather structs containing weather data
// user_settings: Settings struct containing user settings
// returns: array of Weather structs containing weather data only at wanted times
pub fn remove_unwanted_times(day_weather []Weather, user_settings Settings) []Weather {
	mut wanted_times := []Weather{}
	for forecast in day_weather {
		// parse just the hour out of the forecast
		parsed_time := time.parse_iso8601(forecast.date_time) or {
			panic('unable to parse date_time: ${forecast.date_time}')
		}
		local_time := parsed_time.local()
		parsed_hour := local_time.hhmm()[..2].int()
		if parsed_hour < user_settings.daytime_end && parsed_hour >= user_settings.daytime_start {
			wanted_times << forecast
		}
	}

	return wanted_times
}

// get_precipitation uses whatever precipitation mode is stored in settings
// and the precipitation for that day to return a boolean value
// day_weather: array of Weather structs containing weather data
// user_settings: Settings struct containing user settings
// returns: true if weather meets "put on precipitation stuff" threshold, false otherwise
fn get_precipitation(day_weather []Weather, user_settings Settings) bool {
	mut probs := []int{}
	for forecast in day_weather {
		probs << forecast.precipitation_probability
	}

	if user_settings.precipitation_option == 'MAX' {
		return peak_precipitation_probability(probs, user_settings.precipitation_threshold)
	} else if user_settings.precipitation_option == 'SUM' {
		return sum_precipitation_probability(probs, user_settings.precipitation_threshold)
	} else {
		panic('got an invalid precipitation_option: ${user_settings.precipitation_option}')
	}
}

// peak_precipitation_probability is called if the user wants to be told
// to wear rainy/snowy stuff if the probability of rain exceeds a threshold
// at any hour of the day
// probs: array of ints containing the probability of precipitation at hours in the day
// threshold: minimum threshold to be considered a "rainy day"
// returns: true if any probability exceeds the threshold, false otherwise
fn peak_precipitation_probability(probs []int, threshold int) bool {
	for prob in probs {
		if prob > threshold {
			println('rain prob is ${prob}')
			return true
		}
	}
	return false
}

// sum_precipitation_probability is called if the user wants to be told
// to wear rainy/snowy stuff if the sum of probability of rain at each time
// during the day exceeds a certain threshold
// probs: array of ints containing the probability of precipitation at hours in the day
// threshold: minimum threshold to be considered a "rainy day"
// returns: true if sum of probabilities exceeds the threshold, false otherwise
fn sum_precipitation_probability(probs []int, threshold int) bool {
	mut sum := 0
	for prob in probs {
		sum += prob
	}
	println('rain sum is: ${sum}')
	return sum > threshold
}

// get_temperature gives the average temperature during the day.
// By the time this is called, only weather remaining should be
// for times we care about. Just do a straight-up average
// day_weather: array of Weather structs with info on weather
// returns: average temperature for the day
fn get_temperature(day_weather []Weather) f64 {
	mut sum := 0.0
	for forecast in day_weather {
		sum += forecast.temperature
	}
	return sum / day_weather.len
}

fn get_forecast_phrase(day_weather []Weather) string {
	mut counts := map[string]int{}

	for weather in day_weather {
		counts[weather.short_forecast] += 1
	}

	mut max_phrase := ''
	mut max_count := 0

	for key in counts.keys() {
		if counts[key] > max_count {
			max_phrase = key
			max_count = counts[key]
		}
	}

	return max_phrase
}

// get_weather_summ gives the avg temperature, precipiation value,
// and most common icon phrase for the day
pub fn get_weather_summ(day_weather []Weather, user_settings Settings) WeatherSummary {
	new_summ := WeatherSummary{
		avg_temp:        get_temperature(day_weather)
		precip_bool:     get_precipitation(day_weather, user_settings)
		forecast_phrase: get_forecast_phrase(day_weather)
		fahrenheit:      user_settings.fahrenheit
	}

	return new_summ
}

fn get_locked_in_clothes_first(is_rainy bool, wardrobe []Clothing) map[string]Clothing {
	mut variety_count_map := map[string]int{} // how many of each type
	mut outfit := map[string]Clothing{} // the (partial) outfit to return

	if is_rainy {
		internal_wardrobe := wardrobe.filter(it.waterproof)
		// when it's rainy, get the stuff that is waterproof and only has one option
		for item in internal_wardrobe {
			if item.variety in variety_count_map {
				variety_count_map[item.variety] += 1
			} else {
				variety_count_map[item.variety] = 1
			}
		}

		for variety, count in variety_count_map {
			if count == 1 {
				outfit[variety] = internal_wardrobe.filter(it.variety == variety)[0]
			}
		}
	}

	// whether rainy or not, there are some things that just have 1 option (especially in summer)
	for item in wardrobe {
		if item.variety in variety_count_map {
			variety_count_map[item.variety] += 1
		} else {
			variety_count_map[item.variety] = 1
		}
	}

	for variety, count in variety_count_map {
		if count == 1 {
			outfit[variety] = wardrobe.filter(it.variety == variety)[0]
		}
	}

	return outfit
}

fn get_info_on_outfit(outfit map[string]Clothing) (f64, int) {
	mut list_of_selected := []string{}
	mut sum_of_heat_points := 0.0
	mut count_non_none := 0

	for variety, item in outfit {
		list_of_selected << variety

		if item.name != 'none' {
			count_non_none += 1
		}

		sum_of_heat_points += item.heat_points
	}

	return sum_of_heat_points, count_non_none
}

fn get_expected_phrase(goal_temp f64, sum_of_heat_points f64) int {
	// println('goal: $goal_temp \t sohp: $sum_of_heat_points')
	if sum_of_heat_points + 2 < goal_temp {
		return -1
	} else if sum_of_heat_points - 2 > goal_temp {
		return 1
	} else {
		return 0
	}
}

fn calculate_score(distance f64, count int) f64 {
	// for more thoughts about why this is the scoring, see this
	//   desmos graph: https://www.desmos.com/calculator/md9mnpmd0k
	return (distance * distance + 1) * count / 10
}

// get_random_outfit just gets a totally random outfit
fn get_random_outfit(wardrobe []Clothing) map[string]Clothing {
	mut rand_outfit := map[string]Clothing{}
	dup_variety := wardrobe.map(fn (w Clothing) string {
		return w.variety
	})
	uniq_varieties := arrays.distinct(dup_variety)

	for variety in uniq_varieties {
		all_clothes_this_variety := wardrobe.filter(it.variety == variety)

		chosen := rand.element(all_clothes_this_variety) or { panic('CRITICAL ERROR SELECTING RANDOM FOR ${variety}')}

		rand_outfit[variety] = chosen
	}

	return rand_outfit
}

// random_perturb_outfit takes in an outfit and randomly changes one of the articles of clothing
// to something else
fn random_perturb_outfit(wardrobe []Clothing, outfit map[string]Clothing) map[string]Clothing {
	mut variety := rand.element(outfit.keys()) or {panic('CRITICAL ERROR SELECING VARIETY (CODE 01)')}
	mut updated_outfit := outfit.clone()
	for true {
		all_clothes_this_variety := wardrobe.filter(it.variety == variety)

		if all_clothes_this_variety.len != 1 {
			chosen := rand.element(all_clothes_this_variety) or { panic('CRITICAL ERROR SELECTING RANDOM FOR ${variety}')}

			if chosen.clothing_id == updated_outfit[variety].clothing_id {
				// don't want to randomize to the same thing!
				continue
			}

			updated_outfit[variety] = chosen

			return updated_outfit
		}
		variety = rand.element(outfit.keys()) or {panic('CRITICAL ERROR SELECING VARIETY (CODE 02)')}
	}

	return updated_outfit
}

fn use_sim_anneal(wardrobe []Clothing, goal_temp f64) map[string]Clothing {
	mut best_outfit := map[string]Clothing{}
	mut best_score := 100000.0
	mut remaining_trials := 10000

	mut graph_string := ''

	for remaining_trials > 1 { 
		// 1. Get a random outfit
		mut test_outfit := get_random_outfit(wardrobe)
		mut test_budget := rand.int_in_range(10, 1000000/remaining_trials) or { 100 }
		// println('TEST BUDGET THIS RESET: $test_budget')
		mut remaining_on_this_reset := test_budget
		// 2. score the outfit
		sohp, count := get_info_on_outfit(test_outfit)
		distance := math.abs(sohp - goal_temp)
		mut test_score := calculate_score(distance, count)
		// println('TEST SCORE: ${test_score}')
		for remaining_on_this_reset > 0 {
			// 3 if that's the best outfit, make that the best outfit
			if test_score < best_score {
				best_outfit = test_outfit.clone()
				best_score = test_score
				// reward this branch for being our current best
				remaining_on_this_reset *= 2

				new_sohp, new_count := get_info_on_outfit(best_outfit)
				new_distance := math.abs(new_sohp - goal_temp)
				println('*** NEW BEST SCORE: ${best_score}; COUNT = ${new_count}; DIST = ${new_distance}')
				// println(best_outfit)
			}
			// 3b. make a random number of outfits that randomly differ by one clothing item
			mut perturbed_outfits := [random_perturb_outfit(wardrobe, test_outfit)]
			first_sohp, first_count := get_info_on_outfit(perturbed_outfits[0])
			first_distance := math.abs(first_sohp - goal_temp)
			mut scores := [calculate_score(first_distance, first_count)]

			mut num_branches_to_try := rand.int_in_range(10, remaining_on_this_reset/2) or { 10 } 
			// println('NUM BRANCHES THIS BRANCH: ${num_branches_to_try}')
			for _ in 1..num_branches_to_try {
				new_fit := random_perturb_outfit(wardrobe, test_outfit)
				perturbed_outfits << new_fit
				new_sohp, new_count := get_info_on_outfit(new_fit)
				new_distance := math.abs(new_sohp - goal_temp)
				// 3c. score all new outfits, take the best, see if it's better than the parent
				scores << calculate_score(new_distance, new_count)
			}

			// update counters
			remaining_on_this_reset -= num_branches_to_try
			remaining_trials -= num_branches_to_try

			last_best := test_score

			mut found_better := false
			for i, score in scores {
				// 3ci. if one is better than the parent, replace the parent, check if best overall, and loop to 3
				if score < test_score {
					test_score = score
					test_outfit = perturbed_outfits[i].clone()
					found_better = true
				}
			}
			if !found_better {
				// 3cii. if none is better than the parent, break loop, get a new random 
				break
			} else {
				// new_sohp, new_count := get_info_on_outfit(test_outfit)
				// new_distance := math.abs(new_sohp - goal_temp)
				// println('BEST ON THIS BRANCH: ${test_score}; COUNT = ${new_count}; DIST = ${new_distance}')

				// println('${last_best} ${test_score}')
				graph_string += '${last_best} ${test_score}\n'
			}
		}
		// 3ciii. either way, remaining_trials -= 10 (minimum)
		remaining_trials -= 10

		// println('\n\n================== NEW RANDOM ================')
	}

	time_string := time.now().ddmmy()
	os.write_file('dag_stuff/${time_string}_graph_text.txt', graph_string) or { println('WARNING: UNABLE TO WRITE TO FILE') }

	return best_outfit
}

// get_outfit combines temperature and precipitation data to choose the
// correct outfit for the day
// all_weather: array of Weather structs with info on weather for the whole day
// user_settings: struct containing stored user settings
// returns: (array of clothing, expected feedback, and average temperature)
pub fn get_outfit(all_weather []Weather, user_settings Settings) ([]Clothing, int, f64) {
	/*
	options for the algo:
		1. heuristic-ish:
			- sort each clothing type by smallest to largest heat points
			- start with selecting the smallest value for each type. if > goal_temp, that's the best, return
			- otherwise, find how many types clothes have valid options we can pick from
			- divide goal_temp by that number to get a type_goal_temp for each type
			- for each type with options, select the option closest to the type_goal_temp
		2. crazy complicated dynamic programming
			- gonna be a weird variation on the Subset Sum Problem maybe?
		3. do some sort of simulated annealing/random walking
	*/
	day_weather := remove_unwanted_times(all_weather, user_settings)
	// println(all_weather)
	avg_temp := get_temperature(day_weather)
	// println('\navg temp is ${avg_temp}')
	is_rainy := get_precipitation(day_weather, user_settings)
	if !is_rainy && avg_temp > 0 {
		println("GOOD DAY TO BIKE TODAY")
	}
	// get only the clothing options corresponding to the current season
	season := get_season(user_settings)

	mut wardrobe := match season {
		'fall' { user_settings.wardrobe.filter(it.for_fall && it.remaining_count > 0) }
		'winter' { user_settings.wardrobe.filter(it.for_winter && it.remaining_count > 0) }
		'summer' { user_settings.wardrobe.filter(it.for_summer && it.remaining_count > 0) }
		'spring' { user_settings.wardrobe.filter(it.for_fall && it.remaining_count > 0) }
		else { panic('INVALID CURRENT SEASON') }
	}

	goal_temp := user_settings.comfort_temp - avg_temp
	println('goal temp is ${goal_temp}')

	mut outfit := map[string]Clothing{}
	mut sum_of_heat_points := 0.0

	if is_rainy {
		// for each item, if there are any other waterproof items in that variety, only accept if it's waterproof
		just_waterproof := wardrobe.filter(it.waterproof)

		wardrobe = wardrobe.filter(fn [just_waterproof] (x Clothing) bool {
			if just_waterproof.any(it.variety == x.variety) {
				return x.waterproof
			}
			return true
		})
	}

	outfit = use_sim_anneal(wardrobe, goal_temp)
	sum_of_heat_points, _ = get_info_on_outfit(outfit)
	expected_phrase := get_expected_phrase(goal_temp, sum_of_heat_points)

	println('Vetero predicts that you will feel ${expected_phrase} today\n')

	return outfit.values(), expected_phrase, avg_temp
}
